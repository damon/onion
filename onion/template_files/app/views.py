#coding=utf-8

from onion import route
from onion.core.web import RequestHandler

@route('/')
class IndexHandler(RequestHandler):

    def get(self):
        return self.write('hello onion')

